
import ipywidgets as widgets
from IPython.display import HTML, display, clear_output, Javascript, Markdown
from traitlets import CInt, CUnicode
import markdown

colors = ['#f99','#cde','#cc0','#fc2']

def wrap_table(x):
    return '<style>td.largelist { font-size:200%; }</style><table>'+x+'</table>'

def generate_table_row(data,hi,hc):
    clear_output(wait=True)
    content = '<tr>'
    for i in range(len(data)):
        content += '<td  class="largelist" '
        if ( hi.count(i) ):
            content += ' style="background-color:' + colors[hc[hi.index(i)]] +'"'
        content += '>' + str(data[i]) + '</td>'
    content += '</tr>'
    return content

def bubble_sort_display(data):
    x = [ n for n in data]
    frames = []
    final_frames = ''
    frames.append(
        wrap_table(final_frames+generate_table_row(x,[],[]))
    )
    length = len(x)-1
    finished_indices = []
    finished_colors = []
    for i in range(length,0,-1):
        for j in range(i):
            frames.append(
                wrap_table(
                    final_frames+
                    generate_table_row(x, [j,j+1]+finished_indices, [1,1]+finished_colors)
                )
            )
            if x[j] > x[j+1]:
                x[j], x[j+1] = x[j+1], x[j]
                frames.append(
                    wrap_table(                    
                        final_frames+
                        generate_table_row(x,[j,j+1]+finished_indices,[0,0]+finished_colors)
                    )
                )
            frames.append(
                wrap_table(               
                    final_frames
                    +generate_table_row(x,[]+finished_indices,[]+finished_colors)
                )
            )
        finished_indices.insert(0,i)
        finished_colors.insert(0,2)
        frames.append(
            wrap_table(            
                final_frames+
                generate_table_row(x,[]+finished_indices,[]+finished_colors)
            )
        )
        final_frames += generate_table_row(x,[]+finished_indices,[]+finished_colors)

    finished_indices.insert(0,0)
    finished_colors.insert(0,2)
    frames.append(
        wrap_table(        
            final_frames+
            generate_table_row(x,[]+finished_indices,[]+finished_colors)
        )
    )
    return frames

def insertion_sort_display(x):
    frames = []
    final_frames = ''
    frames.append(wrap_table(final_frames+generate_table_row(x,[],[])))
    length = len(x)
    finished_indices = []
    finished_indices.insert(0,0)
    finished_colors = []
    finished_colors.insert(0,2)
    frames.append(
        wrap_table(
            final_frames+
            generate_table_row(x,finished_indices,finished_colors)
        )
    )
    for i in range(1,length):
        finished_indices.append(i)
        finished_colors.insert(0,2)
        b = x[i]
        k = i
        while( k >= 1 and x[k-1] > b):
            frames.append(
                wrap_table(
                    final_frames+
                    generate_table_row(x,[k]+finished_indices,[3]+finished_colors)
                )
            )
            x[k-1],x[k] = x[k],x[k-1]
            frames.append(
                wrap_table(
                    final_frames+
                    generate_table_row(x,[k-1]+finished_indices,[3]+finished_colors)
                )
            )
            k = k-1
        frames.append(
            wrap_table(
                final_frames+
                generate_table_row(x,[]+finished_indices,[]+finished_colors)
            )
        )
        final_frames += generate_table_row(x,[]+finished_indices,[]+finished_colors)
    return frames

def selection_sort_display(x):
    frames = []
    final_frames = ''
    frames.append(wrap_table(final_frames+generate_table_row(x,[],[])))
    length = len(x)-1
    finished_indices = []
    finished_colors = []
    k = -1
    for i in range(length,0,-1):
        k = i
        for j in range(i):
            frames.append(
                wrap_table(
                    final_frames+
                    generate_table_row(x,[j,i]+finished_indices,[1,1]+finished_colors)
                )
            )
            if x[j] > x[k]:
                k = j
                frames.append(
                    wrap_table(
                        final_frames+generate_table_row(x,[i,j]+finished_indices,[0,0]+finished_colors)
                    )
                )
        frames.append(
            wrap_table(
                final_frames+generate_table_row(x,[i,k]+finished_indices,[3,3]+finished_colors)
            )
        )
        x[k], x[i] = x[i], x[k]
        finished_indices.insert(0,i)
        finished_colors.insert(0,2)
        frames.append(
            wrap_table(
                final_frames+
                generate_table_row(x,[]+finished_indices,[]+finished_colors)
            )
        )
        final_frames += generate_table_row(x,[]+finished_indices,[]+finished_colors)

    finished_indices.insert(0,0)
    finished_colors.insert(0,2)
    frames.append(
        wrap_table(
            final_frames+
            generate_table_row(x,[]+finished_indices,[]+finished_colors)
        )
    )
    return frames


def DisplaySort():
    data = [5,4,3,7,2,2,6,0]

    class FrameData(widgets.DOMWidget):
        current_frame = CInt(0, sync=True)
        data_string = CUnicode(('[ '+', '.join(str(x) for x in data))+']', sync=True)
        algo = CUnicode('BubbleSort', sync=True)
        frames = bubble_sort_display(data)
        n_frames = CInt(len(frames), sync=True)

    class Counter(widgets.DOMWidget):
        value = CInt(0, sync=True)

    def print_content():
        text = remove_square_brackets(frameData.data_string)
        try:
            data = [int(n) for n in text.split(',')]
        except:
            return
        if ( frameData.algo == 'BubbleSort' ):
            frameData.frames = bubble_sort_display(data)
            frameData.title = "BubbleSort"
        elif ( frameData.algo == 'InsertionSort' ):
            frameData.frames = insertion_sort_display(data)        
            frameData.title = "InsertionSort"
        else:
            frameData.frames = selection_sort_display(data)
            frameData.title = "SelectionSort"

        on_value_change({'new':0})
        frameData.n_frames = len(frameData.frames)-1
        frameData.current_frame = 0

    def remove_square_brackets(s):
        if s.startswith('['):
            s = s[1:]
        if s.endswith(']'):
            s = s[:-1]
        return s

    def incr(name):
        counter.value += 1 if counter.value < frameData.n_frames-1 else 0

    def decr(name):
        counter.value -= 1 if counter.value > 0 else 0

    def on_value_change(change):
        with out: 
            clear_output(wait=True)
            display(HTML(frameData.frames[change['new']]))

    def on_array_change(change):
        print_content()
    
    def on_algo_change(change):
        frameData.algo = change['new']
        print_content()

    counter = Counter()
    frameData = FrameData()
    
    widgets.Dropdown.value.tag(sync=True)

    algo_select_layout = widgets.Layout(width='250px')
    algo_select = widgets.Dropdown(
        value='BubbleSort',
        placeholder='Choose Algo',
        options=['BubbleSort', 'SelectionSort', 'InsertionSort'],
        description='Algorithm: ',
        style={'description_width':'initial'}, 
        ensure_option=True,
        disabled=False,
        layout=algo_select_layout
    )

    delay_box_layout = widgets.Layout(width='200px')
    delay_box = widgets.IntText(value=500,
                                default=500,
                                description='Delay (in ms): ',
                                style={'description_width':'initial'},
                                disabled=False,
                                layout=delay_box_layout
                               )

    play = widgets.Play(
            value=0,
            min=0,
            max=len(frameData.frames)-1,
            step=1,
            interval=delay_box.value,
            description="Press play",
            disabled=False
        )
    slider = widgets.IntSlider(
            min=0,
            max=len(frameData.frames)-1,
            readout=False
        )

    array_box_layout = widgets.Layout(width='500px')
    array_box = widgets.Text(description='List: ', 
                             style={'description_width':'initial'}, 
                             layout=array_box_layout, value=('[ '+', '.join(str(x) for x in data))+']')

    title_w = widgets.Label(value=frameData.algo)

    out = widgets.Output()


    on_value_change({'new':0})
    print_content()

    layout = widgets.Layout(width='40px')
    button_incr = widgets.Button(description='>',layout=layout)
    button_decr = widgets.Button(description='<',layout=layout)
    button_incr.on_click(incr)
    button_decr.on_click(decr)

    array_box.observe(on_array_change,'value')
    slider.observe(on_value_change, 'value')
    algo_select.observe(on_algo_change, 'value')

    widgets.jslink((delay_box, 'value'), (play, 'interval'))
    widgets.jslink((play, 'value'), (slider, 'value'))
    widgets.jslink((slider, 'value'), (counter, 'value'))
    widgets.jslink((play, 'max'), (frameData, 'n_frames'))
    widgets.jslink((slider, 'max'), (frameData, 'n_frames'))
    widgets.jslink((slider, 'value'), (frameData, 'current_frame'))
    widgets.jslink((array_box, 'value'), (frameData, 'data_string'))
    widgets.jslink((title_w, 'value'), (frameData, 'algo'))


    title = widgets.HTML(markdown.markdown("""# Simple Sorting Algorithms"""))
    subtitle = widgets.HTML(markdown.markdown("""### Input Data"""))
    
    box_play   = widgets.HBox([play, button_decr,button_incr])
    box_slider = widgets.HBox([slider])
    box_control = widgets.VBox([title_w,box_play,box_slider])
    fig = widgets.HBox([box_control,out])
    input_box = widgets.VBox([title,subtitle,array_box,delay_box,algo_select],layout=widgets.Layout(align_items='flex-start'))

    input_box.add_class("input_box")
    box_control.add_class("box_control")
    title_w.add_class("title")
    display(HTML(
         "<style>\
        .input_box {margin-left: 10px;margin-bottom: 100px; margin-right:20px} \
        .box_control { margin-right: 30px;  } \
        .widget-label { font-size: 120%; min-width: 12ex;}\
        .widget-input { font-size: 220%; }\
        .title { font-size: 220%; margin-bottom:20px; }\
        </style>"
    ))

    display(widgets.HBox([input_box,fig]))