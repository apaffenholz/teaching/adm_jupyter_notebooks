from collections import deque
from time import sleep
import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import ipywidgets as widgets
from IPython.display import HTML, display, clear_output
import copy
from io import StringIO
from traitlets import CInt,CUnicode
import json
import markdown

cmap2 = cm.get_cmap('OrRd')
cmap = cm.get_cmap('PuBu')
cmap3 = cm.get_cmap('Blues')


def define_graph(data):
    G = nx.Graph()
    G.add_nodes_from(range(0,len(data["adjacency"])-1))
    for i in data['adjacency'].keys():
        for j in data['adjacency'][i]:
            G.add_edge(int(i),j[0],weight=j[1])

    for n in G.nodes(data=True):
        n[1]['pos'] = data['coordinates'][str(n[0])]
    for e in G.edges(data=True):
        e[2]['width'] = 5
        e[2]['color'] = '#000000'
    return G

def read_graph(filename) :
    with open(filename) as json_file:
        data = json.load(json_file) 

    return define_graph(data["data"])

def draw_graph_mst(G,u,v,high,cycle=[]):
    edict = copy.deepcopy(dict(G.edges()))
    for e in edict:
        if edict[e]['chosen']:
            edict[e]['color'] = '#ff9933'
            edict[e]['width'] = 8
        
    if ( len(cycle) ):
        for f in cycle:
            a = f[0]
            b = f[1]
            if not (a,b) in edict:
                a,b = b,a
            edict[(a,b)]['color'] = '#ff007f'
            edict[(a,b)]['width'] = 12
    
    if ( high ):
        a = u
        b = v
        if not (a,b) in edict:
            a,b = b,a
        edict[(a,b)]['color'] = '#ea8184'
        edict[(a,b)]['color'] = '#11ff11'
        edict[(a,b)]['width'] = 12
        
    graphdata = {}
    graphdata['width'] = [edict[e]['width'] for e in edict]
    graphdata['ecolors'] = [edict[e]['color'] for e in edict]
    graphdata['ncolors'] = [n[1]['color'] for n in G.nodes(data=True)]   
    graphdata['labels'] = nx.get_edge_attributes(G,'weight')

    return graphdata

def find_cycle(G,c,r,x):
    cycle = []
    for n in G.nodes(data=True):
        if n[1]['component'] == c:
            n[1]['parent'] = 'nil'
            n[1]['visited'] = False
            
    Q = deque()
    Q.append(r)
    seen = set()
    seen.add(r)
    while Q:
        v = Q.popleft()
        for wi in G.neighbors(v):
            if G.nodes[wi]['component'] == c and G.edges[v,wi]['chosen'] and not (v == r and wi == x): 
                if wi == x:
                    cycle.append([r,x])
                    G.nodes[wi]['parent'] = v
                    wt = wi
                    while wt != r:
                        cycle.append([G.nodes[wt]['parent'],wt])
                        wt = G.nodes[wt]['parent']
                    return cycle
                if wi not in seen:
                    seen.add(wi)
                    G.nodes[wi]['visited'] = True
                    G.nodes[wi]['parent'] = v
                    Q.append(wi)
    return cycle

def mst_kruskal(G):
    full_mst_data = []

    weights = [(f[2]['weight'],f[0],f[1]) for f in G.edges(data=True)]
    weights.sort()
    
    for n in G.nodes(data=True):
        n[1]['component'] = n[0]
        n[1]['color'] = '#ac003d'

    for e in G.edges(data=True):
        e[2]['chosen'] = False
        e[2]['width'] = 5
        e[2]['color'] = '#000000'

    i = 0
    for w in weights:
        G.edges[(w[1],w[2])]['color'] = cmap3(.3+i*1/40)
        i += 1

    for e in G.edges(data=True):
        e[2]['chosen'] = False

    full_mst_data.append(draw_graph_mst(G,-1,-1,False))

    for t in weights:
        u = t[1]
        v = t[2]
        full_mst_data.append(draw_graph_mst(G,u,v,True))
        if G.nodes[u]['component'] != G.nodes[v]['component']:
            G.edges[(u,v)]['chosen'] = True
            cu = G.nodes[u]['component']
            cv = G.nodes[v]['component']
            col = G.nodes[v]['color']
            for n in G.nodes(data=True):
                if n[1]['component'] == cu:
                    n[1]['component'] = cv
                    n[1]['color'] = col 
        else:
            cycle = find_cycle(G,G.nodes[u]['component'], u, v)
            full_mst_data.append(draw_graph_mst(G,u,v,True,cycle))
            G.edges[(u,v)]['color'] = '#dddddd'
            G.edges[(u,v)]['width'] = 2
        full_mst_data.append(draw_graph_mst(G,u,v,False))
    return full_mst_data


def mst_prim(G,u):
    full_mst_data = []
    for n in G.nodes(data=True):
        n[1]['component'] = n[0]
        n[1]['color'] = '#ac003d'

    for e in G.edges(data=True):
        e[2]['chosen'] = False
        e[2]['width'] = 5
        e[2]['color'] = '#000000'

    weights = [(f[2]['weight'],f[0],f[1]) for f in G.edges(data=True)]
    weights.sort()
    i = 0
    for w in weights:
        G.edges[(w[1],w[2])]['color'] = cmap3(.3+i*1/40)
        i += 1

    G.nodes[u]['color'] = '#88ff22'

    full_mst_data.append(draw_graph_mst(G,-1,-1,False))

    c = G.nodes[u]['component']
    for i in range(0,G.order()-1):
        a_min = 0
        b_min = 0
        nn = 0
        w_min = 100000
        cut_edges = []
        for e in G.edges(data=True):
            a = e[0]
            b = e[1]
            if G.nodes[a]['component'] == c and  G.nodes[b]['component'] != c:
                cut_edges.append((a,b))
                if G.edges[a,b]['weight'] < w_min:
                    a_min = a
                    b_min = b
                    nn = b
                    w_min = G.edges[a,b]['weight']
            if G.nodes[b]['component'] == c and  G.nodes[a]['component'] != c:
                cut_edges.append((a,b))
                if G.edges[a,b]['weight'] < w_min:
                    a_min = a
                    b_min = b
                    nn = a
                    w_min = G.edges[a,b]['weight']
        full_mst_data.append(draw_graph_mst(G,a_min,b_min,False,cut_edges))
        full_mst_data.append(draw_graph_mst(G,a_min,b_min,True,cut_edges))
        G.edges[(a_min,b_min)]['chosen'] = True
        full_mst_data.append(draw_graph_mst(G,u,u,False))
        G.nodes[nn]['component'] = c

    for e in G.edges(data=True):
        if not e[2]['chosen']:
            e[2]['color'] = '#dddddd'
            e[2]['width'] = 2

    full_mst_data.append(draw_graph_mst(G,u,u,False))
    return full_mst_data

graphs = dict()
graphs['G'] = read_graph("mst_graphs/graph01.json")
graphs['H'] = read_graph("mst_graphs/graph02.json")
graphs['K'] = read_graph("mst_graphs/graph03.json")
graphs['small'] = read_graph("mst_graphs/graph04.json")
graph_list = [x for x in graphs.keys()]

def DisplayMST():

    class Counter(widgets.DOMWidget):
        value = CInt(0)
        value.tag(sync=True)

    class FrameData(widgets.DOMWidget):
        G = graphs[graph_list[0]]
        data = mst_kruskal(G)
        n_frames = CInt(len(data))
        n_frames.tag(sync=True)
        current_frame = CInt(0)
        current_frame.tag(sync=True)
        current_graph = CUnicode('G')
        current_graph.tag(sync=True)
        algo = CUnicode('Kruskal')
        algo.tag(sync=True)
        start_node = CInt(0)
        start_node.tag(sync=True)
    
    def show_graph_from_file(file_content):
        content = next(iter(file_content))['content'] #['content']
        print(json.loads(str(content,'utf8')))
    
    def show_graph():
        print(graphs_select.value)
        print(graphs[graphs_select.value])

    def change_input(change):
        content = next(iter(file_picker.value))['content'] #['content']
        data = json.loads(str(content,'utf8'))
        graphs[data['name']] = define_graph(data['data'])
        graph_list.append(data['name'])
        graphs_select.options=tuple(graph_list)
        #graphs_select.value = None
        graphs_select.value = data['name']

    def incr(name):
        counter.value += 1 if counter.value < frameData.n_frames else 0

    def decr(name):
        counter.value -= 1 if counter.value > 0 else 0
    
    out = widgets.Output()

    def draw_graph(n,i):
        clear_output(wait=True)
        plt.figure(figsize=(12,6)) 
        pos = [n[1]['pos'] for n in frameData.G.nodes(data=True)]
        #nx.draw_networkx_nodes(frameData.G, pos=pos, node_size=300, node_color=frameData.data[i]['ncolors'])
        #nx.draw_networkx_edges(frameData.G, pos=pos, )
        nx.draw(
            frameData.G, 
            pos=pos, 
            node_size=300, 
            node_color=frameData.data[i]['ncolors'],
            edge_color=frameData.data[i]['ecolors'], 
            width=frameData.data[i]['width']
        )
        nx.draw_networkx_edge_labels(frameData.G, pos=pos,edge_labels=frameData.data[i]['labels'],font_size=20)
        plt.show()
        

    def on_graph_change(change):
        n = change['new']
        frameData.G = graphs[n]
        if ( algo_select.value == 'Kruskal' ):
            frameData.data = mst_kruskal(frameData.G)
        else:
            frameData.data = mst_prim(frameData.G,0)
        frameData.start_node = 0
        frameData.current_frame = 0
        on_value_change({'new':0})
        frameData.current_graph = n
        frameData.n_frames = len(frameData.data)-1
        start_select.value = 0
        start_select.options = range(frameData.G.number_of_nodes()-1)
    
    def on_value_change(change):
        with out: 
            i=change['new']
            draw_graph(frameData.current_graph,i)    
        
    def on_algo_change(change):
        if ( algo_select.value == 'Kruskal' ):
            frameData.data = mst_kruskal(frameData.G)
            frameData.algo = 'Kruskal'
        else:
            frameData.data = mst_prim(frameData.G,0)
            frameData.start_node = 0
            frameData.algo = 'Prim'
        frameData.current_frame = 0
        frameData.n_frames = len(frameData.data)-1        
        on_value_change({'new':0})
    
    def on_start_change(change):
        if ( algo_select.value == 'Prim' ):
            frameData.data = mst_prim(frameData.G,change['new'])
            frameData.start_node = change['new']
            frameData.current_frame = 0
            on_value_change({'new':0})
            frameData.n_frames = len(frameData.data)-1        
    

    delay = 500
    counter = Counter()
    frameData = FrameData()
    widgets.Dropdown.value.tag(sync=True)

    layout = widgets.Layout(width='40px')
    button_incr = widgets.Button(description='>',layout=layout)
    button_decr = widgets.Button(description='<',layout=layout)
    button_incr.on_click(incr)
    button_decr.on_click(decr)

    play = widgets.Play(
        value=0,
        min=0,
        max=frameData.n_frames-1,
        step=1,
        interval=delay,
        description="Press play",
        disabled=False
    )
    slider = widgets.IntSlider(
        min=0,
        max=frameData.n_frames-1,
        readout=False
    )

    graphs_select=widgets.Dropdown(
        value='G',
        placeholder='Choose Graph',
        options = graph_list,
        description='Graphs:',
        ensure_option=True,
        disabled=False
    )

    delay_box_layout = widgets.Layout(width='200px')
    delay_box = widgets.IntText(value=1000,
                                default=1000,
                                description='Delay (in ms): ',
                                style={'description_width':'initial'},
                                disabled=False,
                                layout=delay_box_layout
                               )

    algo_select_layout = widgets.Layout(width='200px')
    algo_select = widgets.Dropdown(
        value='Kruskal',
        placeholder='Choose Algo',
        options=['Kruskal', 'Prim'],
        description='Algorithm: ',
        style={'description_width':'initial'}, 
        ensure_option=True,
        disabled=False,
        layout=algo_select_layout
    )

    start_select_layout = widgets.Layout(width='300px')
    start_select = widgets.Dropdown(
        value=0,
        placeholder='Start node',
        options=range(frameData.G.number_of_nodes()-1),
        description='Start node (only Prim): ',
        style={'description_width':'initial'}, 
        ensure_option=True,
        disabled=False,
        layout=start_select_layout
    )

    algo_select.observe(on_algo_change, 'value')
    start_select.observe(on_start_change, 'value')

    slider.observe(on_value_change, 'value')

    graphs_select.observe(on_graph_change,'value')
    on_value_change({'new':0})

    widgets.jslink((play, 'value'), (slider, 'value'))
    widgets.jslink((slider, 'value'), (counter, 'value'))
    widgets.jslink((play, 'max'), (frameData, 'n_frames'))
    widgets.jslink((slider, 'max'), (frameData, 'n_frames'))
    widgets.jslink((slider, 'value'), (frameData, 'current_frame'))
    widgets.jslink((delay_box, 'value'), (play, 'interval'))

    box_play   = widgets.HBox([play, button_decr,button_incr])
    box_slider = widgets.HBox([slider])

    control = widgets.VBox([box_play,box_slider])

    file_picker = widgets.FileUpload(accept='*.json')
    file_picker.observe(change_input, 'value')

    title = widgets.HTML(markdown.markdown("""# MST"""))
    subtitle = widgets.HTML(markdown.markdown("""### Input Data"""))
    data_format = widgets.HTML(markdown.markdown(""" 
    Data Format

    {
        "name" : "G",
        "data" :
        {
            "adjacency": {
                "0":[[1,1],[2,7]],
                "1":[[2,5]],
                "2" : []
            },
            "coordinates" :
                {
                    "0":  [0, 0], 
                    "1":  [10, 0], 
                    "2":  [0, 10]
            }
        }
    }
    """))
    graph_widget = widgets.VBox([out])
    graph_widget.add_class('graph_box')
    control.add_class("control")

    display(HTML(
        "<style> \
        .graph_box {margin-left: 10px;margin-bottom: 50px; margin-right:20px;} \
        .control { margin-right: 50px; margin-top: 50px; margin-bottom:50px; } \
        .widget-label { font-size: 120%; min-width: 12ex;}\
        .widget-input { font-size: 220%; }\
        </style>"
    ))

    
    display(widgets.HBox([widgets.VBox([title,subtitle,file_picker,graphs_select,delay_box,algo_select,start_select,control,data_format]), graph_widget]))