# Adm Jupyter Notebooks

## The Subarray Sum Problem

Given a finite list of integers, find a consecutive sublist with maximum total sum of its entries. This can be computed in two ways
  * by enumerating all consecutive sublists and picking the one with the largest sum
  * with Kadane's Algorithm that needs only one iteration over the list

Here is a Binder link to start the notebook: 
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2Fapaffenholz%2Fteaching%2Fadm_jupyter_notebooks/HEAD?urlpath=voila%2Frender%2Fsubarray_sum.ipynb)

Here is a Binder link to start the notebook that just computes runtimes for both algorithms:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2Fapaffenholz%2Fteaching%2Fadm_jupyter_notebooks/HEAD?urlpath=voila%2Frender%2Fsubarray_sum_runtime.ipynb)

## Longest Ascending Subsequence

In a given array, find the longest (strictly) ascending subsequence. This can be done
  * by enumerating all subsequences
  * by dynamic programming: keep the length of the currently known longest sequence ending in each element. For each element we check from left to right if there is a longer sequence ending in an element to its left. 

Here is a Binder link to start the notebook: 
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2Fapaffenholz%2Fteaching%2Fadm_jupyter_notebooks/HEAD?urlpath=voila%2Frender%2Fascending_subsequence.ipynb)

Here is a Binder link to start the notebook that just computes runtimes for both algorithms:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2Fapaffenholz%2Fteaching%2Fadm_jupyter_notebooks/HEAD?urlpath=voila%2Frender%2Fascending_subsequence_runtime.ipynb)

## Simple Sorting Algorithms

Some simple quadratic sorting algorithms to sort a finite list of integers:
  * Bubble Sort
  * Selection Sort
  * Insertion Sort

Here is a Binder link to start the notebook: 
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2Fapaffenholz%2Fteaching%2Fadm_jupyter_notebooks/HEAD?urlpath=voila%2Frender%2Fsimplesort.ipynb)

## MergeSort

Sort an Array with merge sort

Here is a Binder link to start the notebook: 
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2Fapaffenholz%2Fteaching%2Fadm_jupyter_notebooks/HEAD?urlpath=voila%2Frender%2Fmergesort.ipynb)


## Graph Traversal

Traverse a connected component in a graph with Breadth First Search or Depth First Search

Here is a Binder link to start the notebook: 
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2Fapaffenholz%2Fteaching%2Fadm_jupyter_notebooks/HEAD?urlpath=voila%2Frender%2Fgraph_traversal.ipynb)

