from collections import deque
from time import sleep
import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import ipywidgets as widgets
from IPython.display import HTML, display, clear_output
from itertools import chain, combinations
import copy
import sys

cmap2 = cm.get_cmap('OrRd')
cmap = cm.get_cmap('PuBu')
cmap3 = cm.get_cmap('YlGn')

sys.path.insert(1, 'heapdict')
from heapdict import *

def define_graph():
    Adj={0:[[3,2],[4,7]],
         1:[[0,2],[5,1],[6,2]],
         2:[[1,3],[0,3]],
         3:[[4,3],[6,1],[2,1]],
         4:[[7,5]],
         5:[[6,9],[8,2]],
         6:[[8,2],[9,1],[2,4]],
         7:[[9,3],[3,2]],
         8:[[10,2],[14,7]],
         9:[[11,6],[14,3],[3,4]],
         10:[[11,5],[13,8],[15,3]],
         11:[[12,7],[15,8],[7,4]],
         12:[[13,1],[10,6]],
         13:[[8,5]],
         14:[[15,2],[6,3],[10,2]],
         15:[[9,1]]
        }

    pos={0:  ( 0, 30), 
         1:  ( 2, 15), 
         2:  (16, 30), 
         3:  (18, 45),
         4:  ( 6, 54),
         5:  (18,  3),
         6:  (30, 27),
         7:  (32, 63),
         8:  (40,  3),
         9:  (32, 42),
         10: (53, 27),
         11: (44, 57),
         12: (62, 48),
         13: (59, 15),
         14: (41, 23),
         15: (43, 38)
        } 


    G = nx.DiGraph()
    G.add_nodes_from([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15])
    for i in Adj.keys():
        for j in Adj[i]:
            G.add_edge(i,j[0],weight=j[1])
    for n in G.nodes(data=True):
        n[1]['pos'] = pos[n[0]]

    return G

def define_graph_bf():
    Adj={0:[[3,2],[4,2]],
         1:[[0,1],[5,1],[6,2]],
         2:[[1,1],[0,3]],
         3:[[4,5],[6,1],[2,1]],
         4:[[7,5]],
         5:[[6,9],[8,2]],
         6:[[8,2],[9,1],[2,4]],
         7:[[9,3],[3,2]],
         8:[[10,2],[14,7]],
         9:[[11,6],[14,3],[3,4]],
         10:[[11,2],[13,8],[15,3]],
         11:[[12,3],[15,8],[7,4]],
         12:[[13,1],[10,6]],
         13:[[8,5]],
         14:[[15,2],[6,3],[10,2]],
         15:[[9,1]]
        }

    pos={0:  ( 0, 30), 
         1:  ( 2, 15), 
         2:  (16, 30), 
         3:  (18, 45),
         4:  ( 6, 54),
         5:  (18,  3),
         6:  (30, 27),
         7:  (32, 63),
         8:  (40,  3),
         9:  (32, 42),
         10: (53, 27),
         11: (44, 57),
         12: (62, 48),
         13: (59, 15),
         14: (41, 23),
         15: (43, 38)
        } 


    G = nx.DiGraph()
    G.add_nodes_from([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15])
    for i in Adj.keys():
        for j in Adj[i]:
            G.add_edge(i,j[0],weight=j[1])
    for n in G.nodes(data=True):
        n[1]['pos'] = pos[n[0]]

    return G


def draw_graph(G,v,w,high,frames):
    #clear_output(wait=True)
    #labels = nx.get_edge_attributes(G,'weight')
    edict = copy.deepcopy(dict(G.edges()))
    ndict = copy.deepcopy(dict(G.nodes()))
    #plt.figure(figsize=(12,6)) 
    #pos = [n[1]['pos'] for n in G.nodes(data=True)]
    #labels = nx.get_node_attributes(G, 'dist')
    #for l in labels:
    #    if labels[l] == 10000:
    #        labels[l] = 'inf'
    #edgelabels = nx.get_edge_attributes(G,'weight')
    B = []
    for n in G.nodes(data=True):
        if n[1]['dist'] < 1000 and not n[1]['visited']:
            B.append([n[1]['dist'],n[0]])
    B.sort()
    blen = len(B)
    i = 0
    for b in B:
        ndict[b[1]]['color'] = cmap3(.7*(1-i/blen))
        i += 1

    if v != -1:
        ndict[v]['color'] = '#ff0088'
        for u in G.neighbors(v):
            edict[(v,u)]['color'] = "#ffafaf"        
    if w != -1:
        ndict[w]['color'] = '#dd007d'
        if v != -1:
            edict[(v,w)]['color'] = "#dd007d"
        if high == 1:
            edict[(v,w)]['color'] = "#ff007f"            
            ndict[w]['color'] = '#ff887f'
            edict[(v,w)]['width'] = '8'            
        if high == 2:
            edict[(v,w)]['color'] = "#00ff7f"            
            edict[(v,w)]['width'] = '8'            
    ncolors = [ndict[n]['color'] for n in ndict]
    ecolors = [edict[e]['color'] for e in edict]
    width = [float(edict[e]['width']) for e in edict]
    print(width)
    #nx.draw(G, pos=pos, linewidths=2, node_color=ncolors, node_size=1200, edgecolors='black', edge_color=ecolors,width=width,labels=labels)
    #nx.draw_networkx_edge_labels(G, pos=pos,edge_labels=edgelabels,font_size=20) 
    #plt.show()
    frames.append([copy.deepcopy(G),copy.deepcopy(ncolors),copy.deepcopy(ecolors),copy.deepcopy(width)])
    #sleep(s*.01)

def dijkstra(G,u):
    frames = []
    Q = heapdict()
    for n in G.nodes(data=True):
        n[1]['dist'] = 10000
        n[1]['parent'] = 'nil'
        n[1]['visited'] = False
        n[1]['color'] = 'white'
        Q[n[0]] = 1000
    for e in G.edges(data=True):
        e[2]['color'] ='#555555' 
        e[2]['width'] ='4'         

    G.nodes[u]['dist'] = 0
    G.nodes[u]['color'] = cmap2(.8)
    Q[u] = 0
    
    draw_graph(G,-1,-1,0,frames)
    while len(Q)>0:
        (v,d) = Q.popitem()
        G.nodes[v]['color'] = cmap(.8-d/20)
        G.nodes[v]['visited'] = True
        p = G.nodes[v]['parent']
        if p != 'nil':
            G.edges[(p,v)]['color'] = '#ffaa44'
            G.edges[(p,v)]['width'] = '6'
        draw_graph(G,v,-1,0,frames)
        for w in G.neighbors(v):
            if (w in Q):
                draw_graph(G,v,w,0,frames)
                m = min(Q[w],d+G.edges[(v,w)]['weight'])
                if m < Q[w]:
                    draw_graph(G,v,w,1,frames)
                    G.nodes[w]['parent'] = v
                    Q[w] = m
                    G.nodes[w]['dist'] = m
                    draw_graph(G,v,w,1,frames)
                else:
                    draw_graph(G,v,w,2,frames)
                draw_graph(G,v,-1,0,frames)
                
    draw_graph(G,-1,-1,0,frames)
    return display_algo(frames)


def draw_graph_bf(G,v,w,u,high,s,frames):
    #clear_output(wait=True)
    labels = nx.get_edge_attributes(G,'weight')
    edict = copy.deepcopy(dict(G.edges()))
    ndict = copy.deepcopy(dict(G.nodes()))
    #plt.figure(figsize=(12,6)) 
    #pos = [n[1]['pos'] for n in G.nodes(data=True)]
    #labels = nx.get_node_attributes(G, 'dist')
    #for l in labels:
    #    if labels[l] == 10000:
    #        labels[l] = 'inf'
    #edgelabels = nx.get_edge_attributes(G,'weight')
    for n in G.nodes(data=True):
        if n[1]['parent'] != 'nil':
            edict[(n[1]['parent'],n[0])]['color'] = "#ffaa44"            
            edict[(n[1]['parent'],n[0])]['width'] = '8' 
    if v != -1:
        ndict[v]['color'] = '#ff3388'
        if v == 0:
            ndict[v]['color'] = '#ff00ff'
    if w != -1:
        ndict[w]['color'] = '#dd22aa'
        if v != -1:
            edict[(v,w)]['color'] = "#dd007d"
        if high == 1:
            edict[(v,w)]['color'] = "#ff007f"            
            ndict[w]['color'] = '#ff887f'
            edict[(v,w)]['width'] = '8'            
        if u != -1 and high == 2:
            edict[(u,w)]['color'] = "#00ff7f"            
            ndict[u]['color'] = '#00ff7f'
            edict[(u,w)]['width'] = '8'            
    ncolors = [ndict[n]['color'] for n in ndict]
    ecolors = [edict[e]['color'] for e in edict]
    width = [edict[e]['width'] for e in edict]
    #nx.draw(G, pos=pos, linewidths=2, node_color=ncolors, node_size=1400, edgecolors='black', edge_color=ecolors,width=width,labels=labels,font_size=25)
    #nx.draw_networkx_edge_labels(G, pos=pos,edge_labels=edgelabels,font_size=20) 
    #plt.show()
    #sleep(s*.01)
    frames.append([copy.deepcopy(G),copy.deepcopy(ncolors),copy.deepcopy(ecolors),copy.deepcopy(width)])
    return

def draw_frame(frame):
    G = frame[0]
    clear_output(wait=True)
    labels = nx.get_edge_attributes(G,'weight')
    plt.figure(figsize=(12,6)) 
    pos = [n[1]['pos'] for n in G.nodes(data=True)]
    labels = nx.get_node_attributes(G, 'dist')
    for l in labels:
        if labels[l] == 10000:
            labels[l] = 'inf'
    edgelabels = nx.get_edge_attributes(G,'weight')
    ncolors = frame[1]
    ecolors = frame[2]
    width = frame[3]
    nx.draw(G, pos=pos, linewidths=2, node_color=ncolors, node_size=1400, edgecolors='black', edge_color=ecolors,width=width,labels=labels,font_size=22)
    nx.draw_networkx_edge_labels(G, pos=pos,edge_labels=edgelabels,font_size=20) 
    plt.show()
    

index = 0
# general functions
def display_algo(frames,s=1000):
    global index
    index = 0
    out = widgets.Output()
    
    def display_graph(i):
        global index
        index = i
        with out:
            #clear_output(wait=True)
            display(draw_frame(frames[i]))
    
    def on_value_change(change):
            display_graph(change['new'])
            
    def on_button_clicked(b):
        global index
        index += 1
        slider.value = index
        display_graph(index)

    def on_button_clicked_back(b):
        global index
        index -= 1
        slider.value = index
        display_graph(index)

    button = widgets.Button(description="<")
    button.on_click(on_button_clicked_back)
    button2 = widgets.Button(description=">")
    button2.on_click(on_button_clicked)
    #display(button, out)

    play = widgets.Play(
        value=0,
        min=0,
        max=len(frames)-1,
        step=1,
        interval=s,
        description="Press play",
        disabled=False
    )
    slider = widgets.IntSlider(
        min=0,
        max=len(frames)-1,
        readout=False
    )
    slider.observe(on_value_change, 'value')
    on_value_change({'new':0})
    widgets.jslink((play, 'value'), (slider, 'value'))
    vb = widgets.HBox([button, button2, play, slider])
    return widgets.VBox([vb,out])
    
def bellman_ford(G,u,s=1):
    frames = []
    for n in G.nodes(data=True):
        n[1]['dist'] = 10000
        n[1]['parent'] = 'nil'
        n[1]['color'] = 'white'
    for e in G.edges(data=True):
        e[2]['color'] ='#555555' 
        e[2]['width'] ='4'
        
    G.nodes[u]['dist'] = 0
    G.nodes[u]['color'] = cmap2(.8)
    
    draw_graph_bf(G,-1,-1,-1,0,s,frames)
    changes = 0
    for i in range(0,G.order()):
        up = []
        for v in G.nodes():
            draw_graph_bf(G,v,-1,-1,0,s,frames)
            for w in G.neighbors(v):
                e = G.edges[(v,w)]
                if G.nodes[v]['dist'] + e['weight'] < G.nodes[w]['dist']:
                    changes = 1
                    draw_graph_bf(G,v,w,-1,0,2*s,frames)
                    if G.nodes[w]['parent'] != 'nil':
                        draw_graph_bf(G,v,w,G.nodes[w]['parent'],2,4*s,frames)
                    G.nodes[w]['dist'] = G.nodes[v]['dist'] + e['weight']
                    G.nodes[w]['parent'] = v
                    up.append(w)
                    draw_graph_bf(G,v,w,-1,1,4*s,frames)
                    draw_graph_bf(G,v,-1,-1,0,s,frames)

        if changes == 0:
            break
        changes = 0
    draw_graph_bf(G,-1,-1,-1,1,s,frames)
    return display_algo(frames)