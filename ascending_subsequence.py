import ipywidgets as widgets
from IPython.display import HTML, display, clear_output
from traitlets import CInt
import markdown
from itertools import combinations
import time
import random

class AscendingSubsequence(widgets.DOMWidget):
    def __init__(self, data =[2,-3,-5,2,1,3,5]):
        self.data = data
        self.frames = self.longest_ascending_subarray_bf_display(self.data)
        self.n_frames = CInt(len(self.frames)-1)
        self.delay = 1000
        self.algo = 'CompleteEnumeration'
        
    def display_list_subarray_bf(self,data,a,b,max,hm=0):
        colors = ['#ddd','#f99','#cde','#cc0']
        content = ""
        content += "<style>"
        content += "table.arr { float: left; }"
        content += "td.large { font-size:200%; }"
        content += "</style>"
        content += '<div><table width=100% class="arr"><tr>'
        for i in range(len(data)):
            content += '<td class="large" '
            if ( a.count(i) and not b.count(i) ):
                content += ' style="background-color:' + colors[2] +'"'
            if ( b.count(i) ):
                content += ' style="background-color:' + colors[1] +'"'
            content += '>' + str(data[i]) + '</td>'
        content += '<td style="background-color:#fff">&nbsp;</td>'
        content += '<td width=10% class="large" style="background-color:#fff">Max:</td>'
        content += '<td class="large"'
        if ( hm == 0 ):
            content += ' style="background-color:' + '#fff' +'"'
        else:
            content += ' style="background-color:' + colors[3] +'"'
        content += '>'+str(max)+'</td>'
        content += '</tr></table></div>'
        return content

    def display_list_subarray(self,data,length,a,b,c,max,hm=0):
        colors = ['#ddd','#f99','#cde','#cc0']
        content = ""
        content += "<style>"
        content += "table.arr { float: left; }"
        content += "td.large { font-size:200%; }"
        content += "</style>"
        content += '<div><table width=100% class="arr"><tr>'
        for i in range(len(data)):
            content += '<td class="large" '
            if ( a.count(i) and not b.count(i) ):
                content += ' style="background-color:' + colors[2] +'"'
            if ( b.count(i) ):
                content += ' style="background-color:' + colors[1] +'"'
            content += '>' + str(data[i]) + '</td>'
        content += '<td style="background-color:#fff">&nbsp;</td>'
        content += '<td width=10% class="large" style="background-color:#fff">Max:</td>'
        content += '<td class="large"'
        if ( hm == 0 ):
            content += ' style="background-color:' + '#fff' +'"'
        else:
            content += ' style="background-color:' + colors[3] +'"'
        content += '>'+str(max)+'</td>'
        content += '</tr><tr>'
        for i in range(len(data)):
            content += '<td class="large" '
            if ( c.count(i)):
                content += ' style="background-color:' + colors[2] +'"'
            content += '>' + str(length[i]) + '</td>'    
        content += '</tr></table></div>'
        return content

    def longest_ascending_subarray_bf_display(self,a):
        max_len = 0
        frames = []
        y = []
        s = [i for i in range(0,len(a))]
        frames.append(self.display_list_subarray_bf(a,[],[],0))
        for r in range(1,len(a)+1):
            for x in combinations(s, r):
                asc = True
                frames.append(self.display_list_subarray_bf(a,list(x),[],max_len))
                for j in range(0,len(x)-1):
                    if ( a[x[j]] >= a[x[j+1]] ):
                        frames.append(self.display_list_subarray_bf(a,list(x),[x[j],x[j+1]],max_len))
                        asc = False
                        break
                if ( asc and len(x) > max_len ):
                    frames.append(self.display_list_subarray_bf(a,list(x),[],max_len,1))
                    max_len = len(x)
                    y = x
                    frames.append(self.display_list_subarray_bf(a,list(x),[],max_len,1))
                    frames.append(self.display_list_subarray_bf(a,list(x),[],max_len,0))
                    break
        frames.append(self.display_list_subarray_bf(a,list(y),[],max_len,0))
        return frames

    def longest_ascending_subarray_display(self,a):
        length = [1]*len(a)
        frames = []
        frames.append(self.display_list_subarray(a,length,[],[],[],0))
        x = [[a[i]] for i in range(0,len(a))]
        s = [[i] for i in range(0,len(a))]
        #frames.append(display_list_subarray(a,length,[i],[],max(length)))
        for i in range(0,len(a)):
            frames.append(self.display_list_subarray(a,length,[i],[],[],max(length)))
            for j in range(0,i):
                frames.append(self.display_list_subarray(a,length,[i,j],[],[],max(length)))
                if ( a[j] < a[i] and length[i] <= length[j] ):
                    frames.append(self.display_list_subarray(a,length,[],[i,j],[i,j],max(length)))
                    length[i] = length[j] + 1
                    x[i] = x[j].copy()
                    s[i] = s[j].copy()
                    x[i].append(a[i])
                    s[i].append(i)
                    frames.append(self.display_list_subarray(a,length,[],[i,j],[i,j],max(length)))
                    frames.append(self.display_list_subarray(a,length,[i,j],s[i],[],max(length)))
        m = max(length)
        i = length.index(m)
        frames.append(self.display_list_subarray(a,length,s[i],[],[],m))
        return frames

    def display_algo(self):

        out = widgets.Output()
        def on_value_change(change):
            with out: 
                clear_output(wait=True)
                display(HTML(self.frames[change['new']]))

        def remove_square_brackets(s):
            if s.startswith('['):
                s = s[1:]
            if s.endswith(']'):
                s = s[:-1]
            return s

        def on_array_change(change):
            text = remove_square_brackets(array_box.value)
            try:
                self.data = [int(n) for n in text.split(',')]
            except:
                return
            if ( self.algo == 'CompleteEnumeration' ):
                self.frames = self.longest_ascending_subarray_bf_display(self.data)
            else:
                self.frames = self.longest_ascending_subarray_display   (self.data)
            self.n_frames = len(self.frames)-1
            play.max = self.n_frames
            slider.max = self.n_frames
            play.value = 0
            on_value_change({'new':0})

        def on_algo_change(change):
            self.algo = change['new']
            on_array_change({'new':array_box.value})

        def incr(name):
            counter.value += 1 if counter.value < len(self.frames)-1 else 0

        def decr(name):
            counter.value -= 1 if counter.value > 0 else 0

        class Counter(widgets.DOMWidget):
            value = CInt(0, sync=True)  

        counter = Counter()

        play = widgets.Play(
            value=0,
            min=0,
            max=len(self.frames)-1,
            step=1,
            interval=self.delay,
            description="Press play",
            disabled=False
        )
        slider = widgets.IntSlider(
            min=0,
            max=len(self.frames)-1,
            readout=False
        )

        array_box_layout = widgets.Layout(width='500px')
        array_box = widgets.Text(description='List: ', 
                                 style={'description_width':'100px'}, 
                                 layout=array_box_layout, 
                                 value=('[ '+', '.join(str(x) for x in self.data))+']'
                                )
        
        delay_box_layout = widgets.Layout(width='200px')
        delay_box = widgets.IntText(value=200,
                                    default=200,
                                    description='Delay (in ms): ',
                                    style={'description_width':'100px'},
                                    disabled=False,
                                    layout=delay_box_layout
                                   )

        algo_select_layout = widgets.Layout(width='300px')
        algo_select = widgets.Dropdown(
            value='CompleteEnumeration',
            placeholder='Choose Algo: ',
            options=['CompleteEnumeration', 'Dynamic Programming'],
            description='Algorithm: ',
            style={'description_width':'100px'}, 
            ensure_option=True,
            disabled=False,
            layout=algo_select_layout
        )

        layout = widgets.Layout(width='40px')
        button_incr = widgets.Button(description='>',layout=layout)
        button_decr = widgets.Button(description='<',layout=layout)
        button_incr.on_click(incr)
        button_decr.on_click(decr)

        widgets.Dropdown.value.tag(sync=True)

        slider.observe(on_value_change, 'value')
        array_box.observe(on_array_change, 'value')
        algo_select.observe(on_algo_change, 'value')
        on_value_change({'new':0})
        widgets.jslink((play, 'value'), (slider, 'value'))
        widgets.jslink((delay_box, 'value'), (play, 'interval'))
        widgets.jslink((slider, 'value'), (counter, 'value'))

        title1 = widgets.HTML(markdown.markdown("""# Longest Ascending"""))
        title2 = widgets.HTML(markdown.markdown("""#      Subsequence Problem"""))
        subtitle = widgets.HTML(markdown.markdown("""### Input Data"""))
        algo_title = widgets.HTML(markdown.markdown("""# Visualization"""))
        input_box = widgets.VBox([array_box, delay_box ,algo_select])
        vb2 = widgets.HBox([play,button_decr,button_incr])
        box_control = widgets.VBox([vb2,slider])
        fig = widgets.VBox([title1,title2,subtitle,input_box,algo_title,box_control,out])

        input_box.add_class("input_box")
        box_control.add_class("box_control")
        algo_title.add_class("algo_title")
        title1.add_class("pagetitle_t")
        title2.add_class("pagetitle_b")
        #delay_box.add_class("delay_box")
        fig.add_class("fullpage")

        customHTML= widgets.HTML(
            """<style>\
             @import url('https://fonts.googleapis.com/css?family=Raleway:300,400,700&display=swap');\
            .pagetitle_t { font-family: \"Raleway\", sans-serif !important; font-size: 150%; color: rgb(185,15,84); margin-bottom: -30px;} \
            .pagetitle_b { font-family: \"Raleway\", sans-serif !important; font-size: 150%; color: rgb(185,15,84); margin-left: 30px;} \
            .fullpage { min-width: 700px; max-width: 1000px;} \
            .input_box {margin-left: 10px;margin-bottom: 100px;} \
            .box_control { margin-left: 0px; margin-bottom: 20px; margin-top: 30px; } \
            .delay_box { margin-left: 10px; } \
            .widget-label { font-size: 120%; min-width: 12ex;}\
            .widget-input { font-size: 220%; }\
            .title { font-size: 220%; margin-bottom:20px; }\
            .algo_title { font-size: 120%; margin-bottom:20px; margin-top:20px; }\
            </style>"""
        )

        res = widgets.VBox([customHTML,fig])
        return res
    
class AscendingSubsequenceRuntime(widgets.DOMWidget):
    def __init__(self, data =[2,-3,-5,2,1,3,5]):
        self.data = data
        start = time.time()
        self.longest_ascending_subarray_bf(self.data)
        end = time.time()
        self.runtime_ce = end-start
        start = time.time()
        self.longest_ascending_subarray_dp(self.data)
        end = time.time()
        self.runtime_dp = end-start



    def longest_ascending_subarray_bf(self,a):
        max_len = 0
        y = []
        for r in range(1,len(a)+1):
            for x in combinations(a, r):
                asc = True
                for j in range(0,len(x)-1):
                    if ( x[j] >= x[j+1] ):
                        asc = False
                        break
                if ( asc and len(x) > max_len ):
                    max_len = len(x)
                    y = x
        return [max_len, y]

    def longest_ascending_subarray_dp(self,a):
        length = [1]*len(a)
        length[0] = 1
        x = [[a[i]] for i in range(0,len(a))]
        for i in range(1,len(a)):
            for j in range(0,i):
                if ( a[j] < a[i] and length[i] <= length[j] ):
                    length[i] = length[j] + 1
                    x[i] = x[j].copy()
                    x[i].append(a[i])
        m = max(length)
        i = length.index(m)
        return [m,x[i]]
        
    def display_res(self):
        colors = ['#ddd','#f99','#cde','#cc0']
        content = ""
        content += "<style>"
        content += "table.arr { float: left; }"
        content += "td.large { font-size:200%; }"
        content += "</style>"
        content += '<div><table width=100% class="arr"><tr>'
        content += '<td class="large">Complete Enumeration: </td>'
        start = time.time()
        self.longest_ascending_subarray_bf(self.data)
        end = time.time()
        diff = end-start
        content += '<td class="large">'+"{0:0.5f}".format(diff)+'</td>'
        content += '</tr>'
        content += '<td class="large">Dynamic Programming: </td>'
        start = time.time()
        self.longest_ascending_subarray_dp(self.data)
        end = time.time()
        diff = end-start
        content += '<td class="large">'+"{0:0.5f}".format(diff)+'</td>'
        content += '</table></div>'

        return content
    
    def display_algo(self):

        out = widgets.Output()

        def new_random_array():
            self.data = [random.randrange(0, 50, 1) for i in range(elements.value)]
            array_box.value=('[ '+', '.join(str(x) for x in self.data))+']'

        def on_array_change(change):
            text = remove_square_brackets(array_box.value)
            try:
                self.data = [int(n) for n in text.split(',')]
            except:
                return

            elements.value = len(self.data)
            with out: 
                clear_output(wait=True)
                res = self.display_res()
                display(HTML(res))

        def remove_square_brackets(s):
            if s.startswith('['):
                s = s[1:]
            if s.endswith(']'):
                s = s[:-1]
            return s

        button = widgets.Button(
            description='Recompute', 
            button_style='success', 
            layout = widgets.Layout(width='150px')
        )
        elements = widgets.BoundedIntText(
            style={'description_width':'100px'}, 
            value=7,
            min=0,
            max=25,
            description='# Elements:',
            disabled=False,
            layout = widgets.Layout(width='200px')
        )

        array_box_layout = widgets.Layout(width='700px')
        array_box = widgets.Text(description='List: ', 
                                 style={'description_width':'100px'}, 
                                 layout=array_box_layout, 
                                 value=('[ '+', '.join(str(x) for x in self.data))+']'
                                )
        
        array_box.observe(on_array_change, 'value')
        on_array_change({'new':array_box.value})

        button.on_click(lambda btn: new_random_array())

        title1 = widgets.HTML(markdown.markdown("""# Longest Ascending"""))
        title2 = widgets.HTML(markdown.markdown("""#      Subsequence Problem (Runtime)"""))
        subtitle = widgets.HTML(markdown.markdown("""### Input Data"""))

        algo_title = widgets.HTML(markdown.markdown("""# Runtime"""))
        fig = widgets.VBox([title1,title2,subtitle,widgets.HBox([elements,button]),algo_title,array_box,out])

        algo_title.add_class("algo_title")
        title1.add_class("pagetitle_t")
        title2.add_class("pagetitle_b")
        fig.add_class("fullpage")

        customHTML= widgets.HTML(
            """<style>\
             @import url('https://fonts.googleapis.com/css?family=Raleway:300,400,700&display=swap');\
            .pagetitle_t { font-family: \"Raleway\", sans-serif !important; font-size: 150%; color: rgb(185,15,84); margin-bottom: -30px;} \
            .pagetitle_b { font-family: \"Raleway\", sans-serif !important; font-size: 150%; color: rgb(185,15,84); margin-left: 30px;} \
            .fullpage { min-width: 700px; max-width: 1000px;} \
            .input_box {margin-left: 10px;margin-bottom: 100px;} \
            .box_control { margin-left: 0px; margin-bottom: 20px; margin-top: 30px; } \
            .delay_box { margin-left: 10px; } \
            .widget-label { font-size: 120%; min-width: 12ex;}\
            .widget-input { font-size: 220%; }\
            .title { font-size: 220%; margin-bottom:20px; }\
            .algo_title { font-size: 120%; margin-bottom:20px; margin-top:20px; }\
            </style>"""
        )

        res = widgets.VBox([customHTML,fig])
        return res