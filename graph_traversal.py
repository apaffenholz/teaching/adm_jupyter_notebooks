from collections import deque
import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import ipywidgets as widgets
from IPython.display import HTML, display, clear_output
import copy
from traitlets import CInt,CUnicode
import json
import markdown

cmap2 = cm.get_cmap('OrRd')
cmap = cm.get_cmap('PuBu')

def define_graph(data):
    G = nx.Graph()
    G.add_nodes_from(range(0,len(data["adjacency"])-1))
    for i in data['adjacency'].keys():
        for j in data['adjacency'][i]:
            G.add_edge(int(i),j)

    for n in G.nodes(data=True):
        n[1]['pos'] = data['coordinates'][str(n[0])]
    for e in G.edges(data=True):
        e[2]['width'] = 5
        e[2]['color'] = '#000000'
    return G

def read_graph(filename) :
    with open(filename) as json_file:
        data = json.load(json_file) 

    return define_graph(data["data"])

graphs = dict()
graphs['VL'] = read_graph("gt_graphs/gt_graph03.json")
graphs['G'] = read_graph("gt_graphs/gt_graph01.json")
graphs['H'] = read_graph("gt_graphs/gt_graph02.json")
graphs['small'] = read_graph("gt_graphs/gt_simple.json")
graphs['disconnected'] = read_graph("gt_graphs/gt_disconnected.json")
graph_list = ['VL','G','H','small','disconnected']

def updatecolors(G,Q,v):
    ncolors = [n[1]['color'] for n in G.nodes(data=True)]
    i = 0
    s = len(Q)
    for w in Q:
        ncolors[w] = cmap(.6-i*.6/len(Q))
        i += 1
    if v != -1:
        ncolors[v] = '#ff007f'
    return ncolors

def draw_graph_bfs(G,v,w,high,ncolors):
    if v != w:
        ec = G.get_edge_data(v,w)['color']
    if high:
        if v == w:
            for u in G.neighbors(v):
                G.get_edge_data(u,v)['color'] = '#ff007f'                
        else:
            G.get_edge_data(v,w)['color'] = '#80ff00'
    pos = [n[1]['pos'] for n in G.nodes(data=True)]

    graphdata = {}
    graphdata['width'] = [float(e[2]['width']) for e in G.edges(data=True)]
    graphdata['ecolors'] = [e[2]['color'] for e in G.edges(data=True)]
    graphdata['ncolors'] = ncolors
    graphdata['labels'] = nx.get_node_attributes(G, 'level')
    graphdata['num'] = nx.get_node_attributes(G, 'num')
    if v != w:
        G.get_edge_data(v,w)['color'] = ec

    return graphdata

def BFS(G,u):
    full_traversal_data = []
    num = 0
    for n in G.nodes(data=True):
        n[1]['level'] = 'inf'
        n[1]['num'] = 'inf'
        n[1]['parent'] = 'nil'
        n[1]['color'] = 'white'
        G.nodes[u]['level'] = 0
        G.nodes[u]['color'] = cmap2(.6)
    
    for e in G.edges(data=True):
        e[2]['color'] ='#555555' 
        e[2]['width'] ='4'         
    
    Q = deque()
    ncolors = updatecolors(G,Q,u)
    full_traversal_data.append(draw_graph_bfs(G,u,u,False,ncolors))

    Q.append(u)
    G.nodes[u]['num'] = num
    num = num+1
    ncolors = updatecolors(G,Q,u)
    nodes_updated=1
    while Q and nodes_updated < G.order():
        v = Q.popleft()
        ncolors = updatecolors(G,Q,v)
        full_traversal_data.append(draw_graph_bfs(G,v,v,True,ncolors))
        for w in G.neighbors(v):
            full_traversal_data.append(draw_graph_bfs(G,v,w,True,ncolors))
            if G.nodes[w]['level'] == 'inf':
                G.nodes[w]['level'] = G.nodes[v]['level']+1
                G.nodes[w]['parent'] = v
                nodes_updated += 1
                G.nodes[w]['color'] = cmap2(.8-G.nodes[w]['level']*1/5)
                G.get_edge_data(v,w)['color'] = '#ff9933'
                G.get_edge_data(v,w)['width'] = 8
                Q.append(w)
                G.nodes[w]['num'] = num
                num = num+1
                ncolors = updatecolors(G,Q,v)
            else:
                if G.nodes[v]['parent'] == w:
                    G.get_edge_data(v,w)['color'] = '#ff9933'
                    G.get_edge_data(v,w)['width'] = 8
                else:
                    G.get_edge_data(v,w)['color'] = '#999999'
                    G.get_edge_data(v,w)['width'] = 2      
            full_traversal_data.append(draw_graph_bfs(G,v,w,False,ncolors))
        
    Q.clear()
    for e in G.edges(data=True):
        if e[2]['width'] == '4':         
            e[2]['color'] ='#999999' 
            e[2]['width'] ='2'         
    ncolors = updatecolors(G,Q,-1)
    full_traversal_data.append(draw_graph_bfs(G,v,w,False,ncolors))

    return full_traversal_data


def draw_graph_dfs(G,u,high):
    edict = copy.deepcopy(dict(G.edges()))
    for n in G.nodes(data=True):
        if n[1]['parent'] != 'nil' and n[1]['visited'] == True:
            a = n[0]
            b = n[1]['parent']
            if not (a,b) in edict:
                a,b = b,a
            edict[(a,b)]['color'] = '#ff9933'
            edict[(a,b)]['width'] = 8
    
    ncolors = [n[1]['color'] for n in G.nodes(data=True)]
    if high:
        ncolors[u] = '#00ff00'
    
    graphdata = {}
    graphdata['width'] = [float(edict[e]['width']) for e in edict]
    graphdata['ecolors'] = [edict[e]['color'] for e in edict]
    graphdata['ncolors'] = ncolors
    graphdata['labels'] = nx.get_node_attributes(G, 'level')
    graphdata['num'] = nx.get_node_attributes(G, 'num')

    return graphdata


def DFS_visit(G,u,num,ftd):
    G.nodes[u]['visited'] = True    
    for w in G.neighbors(u):
        if not G.nodes[w]['visited']:
            G.nodes[w]['level'] = G.nodes[u]['level']+1
            num = num+1
            G.nodes[w]['num'] = num
            G.nodes[w]['parent'] = u
            G.nodes[w]['color'] = cmap2(.6-G.nodes[w]['level']*1/40)
            G.get_edge_data(u,w)['color'] = '#ff9933'
            G.get_edge_data(u,w)['width'] = 8
            ftd.append(draw_graph_dfs(G,w,True))
            num = DFS_visit(G,w,num,ftd)
            ftd.append(draw_graph_dfs(G,u,True))
    return num
    
            
def DFS(G,u):
    full_traversal_data = []
    for n in G.nodes(data=True):
        n[1]['level'] = 'inf'
        n[1]['num'] = 'inf'
        n[1]['parent'] = 'nil'
        n[1]['color'] = '#22aaff'
        n[1]['visited'] = False
        
    G.nodes[u]['level'] = 0
    G.nodes[u]['num'] = 0
    G.nodes[u]['color'] = cmap2(.6)
    
    for e in G.edges(data=True):
        e[2]['color'] ='#555555' 
        e[2]['width'] =4         
    
    full_traversal_data.append(draw_graph_dfs(G,u,True))
    DFS_visit(G,u,0,full_traversal_data)
    full_traversal_data.append(draw_graph_dfs(G,u,False))
    return full_traversal_data

def DisplayGraphTraversal():

    class Counter(widgets.DOMWidget):
        value = CInt(0)
        value.tag(sync=True)

    class FrameData(widgets.DOMWidget):
        G = graphs[graph_list[0]]
        data = BFS(G,0)
        n_frames = CInt(len(data))
        n_frames.tag(sync=True)
        current_frame = CInt(0)
        current_frame.tag(sync=True)
        current_graph = CUnicode('VL')
        current_graph.tag(sync=True)
        label = CUnicode('level')
        label.tag(sync=True)
        algo = CUnicode('BFS')
        algo.tag(sync=True)
        start_node = CInt(0)
        start_node.tag(sync=True)
    
    def show_graph_from_file(file_content):
        content = next(iter(file_content))['content'] #['content']
        print(json.loads(str(content,'utf8')))
    
    def show_graph():
        print(graphs_select.value)
        print(graphs[graphs_select.value])

    def change_input(change):
        content = next(iter(file_picker.value))['content'] #['content']
        data = json.loads(str(content,'utf8'))
        graphs[data['name']] = define_graph(data['data'])
        graph_list.append(data['name'])
        graphs_select.options=tuple(graph_list)
        #graphs_select.value = None
        graphs_select.value = data['name']

    def incr(name):
        counter.value += 1 if counter.value < frameData.n_frames else 0

    def decr(name):
        counter.value -= 1 if counter.value > 0 else 0
    
    out = widgets.Output()

    def draw_graph(n,i):
        clear_output(wait=True)
        plt.figure(figsize=(12,6)) 
        pos = [n[1]['pos'] for n in frameData.G.nodes(data=True)]
        if frameData.label == 'level':
            labels = frameData.data[i]['labels']
        else:
            labels = frameData.data[i]['num']
        nx.draw(frameData.G, pos=pos, linewidths=2, 
                edgecolors='black', 
                edge_color=frameData.data[i]['ecolors'], 
                labels=labels,
                font_size='18', 
                font_weight='normal', 
                width=frameData.data[i]['width'], 
                node_size=1200,
                node_color=frameData.data[i]['ncolors']
        )
        plt.show()
        

    def on_graph_change(change):
        n = change['new']
        frameData.G = graphs[n]
        if (frameData.algo == 'BFS' ):
            frameData.data = BFS(frameData.G,0)
            frameData.label = 'level'
        else:
            frameData.data = DFS(frameData.G,0)
            frameData.label = 'order'
        frameData.start_node = 0
        frameData.current_frame = 0
        frameData.current_graph = n
        frameData.n_frames = len(frameData.data)-1
        start_select.value = 0
        start_select.options = range(frameData.G.number_of_nodes())        
        on_value_change({'new':0})
    
    def on_value_change(change):
        with out: 
            i=change['new']
            draw_graph(frameData.current_graph,i)    
        
    def on_algo_change(change):
        if ( algo_select.value == 'BFS' ):
            frameData.data = BFS(frameData.G,frameData.start_node)
            frameData.algo = 'BFS'
            frameData.label = 'level'
        else:
            frameData.data = DFS(frameData.G,frameData.start_node)
            frameData.algo = 'DFS'
            frameData.label = 'order'
        frameData.current_frame = 0
        frameData.n_frames = len(frameData.data)-1        
        on_value_change({'new':0})
    
    def on_start_change(change):
        if ( algo_select.value == 'BFS' ):
            frameData.data = BFS(frameData.G,change['new'])
        else:
            frameData.data = DFS(frameData.G,change['new'])
        frameData.start_node = change['new']
        frameData.current_frame = 0
        frameData.n_frames = len(frameData.data)-1     
        on_value_change({'new':0})

    def on_label_change(change):
        on_value_change({'new':frameData.current_frame})    
    

    delay = 1000
    counter = Counter()
    frameData = FrameData()
    widgets.Dropdown.value.tag(sync=True)

    layout = widgets.Layout(width='40px')
    button_incr = widgets.Button(description='>',layout=layout)
    button_decr = widgets.Button(description='<',layout=layout)
    button_incr.on_click(incr)
    button_decr.on_click(decr)

    play = widgets.Play(
        value=0,
        min=0,
        max=frameData.n_frames-1,
        step=1,
        interval=delay,
        description="Press play",
        disabled=False
    )
    slider = widgets.IntSlider(
        min=0,
        max=frameData.n_frames-1,
        readout=False
    )

    graphs_select=widgets.Dropdown(
        value='VL',
        default='VL',
        placeholder='Choose Graph',
        options = graph_list,
        description='Graphs:',
        ensure_option=True,
        disabled=False
    )

    delay_box_layout = widgets.Layout(width='200px')
    delay_box = widgets.IntText(value=1000,
                                default=1000,
                                description='Delay (in ms): ',
                                style={'description_width':'initial'},
                                disabled=False,
                                layout=delay_box_layout
                               )

    algo_select_layout = widgets.Layout(width='250px')
    algo_select = widgets.Dropdown(
        value='BFS',
        placeholder='Choose Algo',
        options=['BFS', 'DFS'],
        description='Algorithm: ',
        style={'description_width':'initial'}, 
        ensure_option=True,
        disabled=False,
        layout=algo_select_layout
    )

    start_select_layout = widgets.Layout(width='250px')
    start_select = widgets.Dropdown(
        value=0,
        placeholder='Start node',
        options=range(frameData.G.number_of_nodes()),
        description='Start node: ',
        style={'description_width':'initial'}, 
        ensure_option=True,
        disabled=False,
        layout=start_select_layout
    )

    label_select_layout = widgets.Layout(width='250px')
    label_select = widgets.Dropdown(
        value='level',
        placeholder='Show level/order',
        options=['level', 'order'],
        description='Show level/order: ',
        style={'description_width':'initial'}, 
        ensure_option=True,
        disabled=False,
        layout=label_select_layout
    )


    algo_select.observe(on_algo_change, 'value')
    start_select.observe(on_start_change, 'value')
    label_select.observe(on_label_change, 'value')

    slider.observe(on_value_change, 'value')
    on_value_change({'new':0})

    graphs_select.observe(on_graph_change,'value')
    on_graph_change({'new':'VL'})

    widgets.jslink((play, 'value'), (slider, 'value'))
    widgets.jslink((slider, 'value'), (counter, 'value'))
    widgets.jslink((play, 'max'), (frameData, 'n_frames'))
    widgets.jslink((slider, 'max'), (frameData, 'n_frames'))
    widgets.jslink((slider, 'value'), (frameData, 'current_frame'))
    widgets.jslink((delay_box, 'value'), (play, 'interval'))
    widgets.jslink((label_select, 'value'), (frameData, 'label'))
    widgets.jslink((graphs_select, 'value'), (frameData, 'current_graph'))


    title = widgets.HTML(markdown.markdown("""# Graph Traversal"""))
    subtitle = widgets.HTML(markdown.markdown("""### Input Data"""))

    data_format = widgets.HTML(markdown.markdown(""" 
    Data Format

    {
        "name" : "G",
        "data" :
        {
            "adjacency": {
                "0":[[1,2],
                "1":[2],
                "2" : []
            },
            "coordinates" :
                {
                    "0":  [0, 0], 
                    "1":  [10, 0], 
                    "2":  [0, 10]
            }
        }
    }
    """))

    box_play   = widgets.HBox([play, button_decr,button_incr])
    box_slider = widgets.HBox([slider])
    control = widgets.VBox([box_play,box_slider])

    file_picker = widgets.FileUpload(accept='*.json')
    file_picker.observe(change_input, 'value')
    graph_box = widgets.HBox([file_picker,graphs_select])
    data_box = widgets.VBox([title,subtitle,graph_box,delay_box,algo_select,start_select,label_select,control,data_format])

    graph_box.add_class("graph_box")
    control.add_class("control")

    display(HTML(
         "<style>\
        .graph_box {margin-left: 10px;margin-bottom: 50px; margin-right:20px;} \
        .control { margin-right: 50px; margin-top: 50px; margin-bottom:50px; } \
        .widget-label { font-size: 120%; min-width: 12ex;}\
        .widget-input { font-size: 220%; }\
        </style>"
    ))

    display(widgets.HBox([data_box,out]))