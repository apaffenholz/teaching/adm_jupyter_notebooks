import ipywidgets as widgets
from IPython.display import HTML, display, clear_output
from traitlets import CInt
import markdown
import time
import random

class SubarraySum(widgets.DOMWidget):
    def __init__(self, data =[2,-5,1,2,-4,2,3,5,-3]):
        self.data = data
        self.frames = self._subarray_sum_impl_display(self.data)
        self.n_frames = CInt(len(self.frames)-1)
        self.delay = 1000
        self.algo = 'CompleteEnumeration'

    # subarray sum via consecutive arrays
    def _subarray_sum_display_list(self,data,a,b,indc,hi,hc,s=1):
        l = 0
        colors = ['#ddd','#f99','#cde','#cc0']
        content = ""
        content += "<style>"
        content += "table.arr { float: left; }"
        content += "td.large { font-size:200%; }"
        content += "</style>"
        content += '<div><table width=100% class="arr"><tr>'
        for i in range(len(data)):
            content += '<td class="large" '
            if ( hi.count(i) ):
                l = i
                content += ' style="background-color:' + colors[hc[hi.index(i)]] +'"'
            content += '>' + str(data[i]) + '</td>'
        content += '<td style="background-color:#fff">&nbsp;</td>'
        content += '<td width=10% class="large" style="background-color:#fff">Max:</td>'
        content += '<td class="large"'
        content += ' style="background-color:' + colors[indc[1]] +'"'
        content += '>'+str(b)+'</td>'
        content += '</tr>'
        content += '<tr>'
        for i in range(l):
            content += '<td style="background-color:#fff">&nbsp;</td>'
        content += '<td class="large"'
        content += ' style="background-color:' + colors[indc[0]] +'"'
        content += '>'+str(a)+'</td>'
        content += '</tr>'
        content += '</table></div>'
        return content

    def _subarray_sum_impl_display(self,data):
        max_sum = 0
        cur_sum = 0
        frames = []
        min = 0
        max = 0
        length = len(data)
        frames.append(self._subarray_sum_display_list(data,data[0],max_sum,[0,0],[],[]))
        for i in range(length):
            cur_sum = 0
            for j in range(i,length):
                cur_sum += data[j]
                frames.append(self._subarray_sum_display_list(data,cur_sum,max_sum,[0,0],range(i,j+1),[1] * (j-i+1)))
                if ( cur_sum > max_sum ) :
                    min = i
                    max = j
                    frames.append(self._subarray_sum_display_list(data,cur_sum,max_sum,[3,0],range(i,j+1),[1] * (j-i+1)))
                    max_sum = cur_sum
                    frames.append(self._subarray_sum_display_list(data,cur_sum,max_sum,[0,3],range(i,j+1),[1] * (j-i+1)))
        frames.append(self._subarray_sum_display_list(data,max_sum,max_sum,[0,0],range(min,max+1),[1]*(max-min+1)))
        return frames

# subarray sum via consecutive arrays
    def _kadane_display_list(self,data,a,b,indc,cp,hi,hc):
        l = 0
        colors = ['#ddd','#f99','#cde','#cc0']
        content = ""
        content += "<style>"
        content += "table.arr { float: left; }"
        content += "td.large { font-size:200%; }"
        content += "</style>"
        content += '<div><table width=100% class="arr"><tr>'
        for i in range(len(data)):
            content += '<td class="large" '
            if ( hi.count(i) ):
                l = i
                content += ' style="background-color:' + colors[hc[hi.index(i)]] +'"'
            content += '>' + str(data[i]) + '</td>'
        content += '<td style="background-color:#fff">&nbsp;</td>'
        content += '<td width=10% class="large" style="background-color:#fff">Max:</td>'
        content += '<td class="large"'
        content += ' style="background-color:' + colors[indc[1]] +'"'
        content += '>'+str(b)+'</td>'
        content += '</tr>'
        content += '<tr>'
        if ( cp == -1 ):
            content += '<td style="background-color:#fff; color:#fff;">&nbsp;a</td>'
        else:
            for i in range(cp-1):
                content += '<td style="background-color:#fff">&nbsp;</td>'
            content += '<td class="large"'
            content += ' style="background-color:' + colors[indc[0]] +'"'
            content += '>'+str(a)+'</td>'
        content += '</tr>'
        content += '</table></div>'
        return content

    def _kadane_impl_display(self,data):
        max_sum = 0
        cur_sum = 0
        frames = []
        max_range = []
        j = 0
        length = len(data)
        frames.append(self._kadane_display_list(data,cur_sum,max_sum,[0,0],-1,[],[]))
        for i in range(length):
            r = range(j,i+1)
            c = [1]*(i-j+1)
            c[i-j] = 2
            frames.append(self._kadane_display_list(data,cur_sum,max_sum,[0,0],i,r,c))
            c[i-j] = 1
            if ( cur_sum+data[i] < 0 ):
                cur_sum = 0
                j = i+1
                r = range(j,i+1)
                c = [1]*(i-j+1)
            else:
                cur_sum = cur_sum+data[i]
            frames.append(self._kadane_display_list(data,cur_sum,max_sum,[1,0],i+1,r,c))
            if ( cur_sum > max_sum ):
                frames.append(self._kadane_display_list(data,cur_sum,max_sum,[3,0],i+1,r,c))
                max_sum = cur_sum
                max_range = r
                frames.append(self._kadane_display_list(data,cur_sum,max_sum,[0,3],i+1,r,c))
        cols = [1 for i in max_range]
        frames.append(self._kadane_display_list(data,max_sum,max_sum,[0,1],max_range[len(max_range)-1]+1,max_range,cols))
        return frames


    # general functions
    def display_algo(self):

        out = widgets.Output()
        def on_value_change(change):
            with out: 
                clear_output(wait=True)
                display(HTML(self.frames[change['new']]))

        def remove_square_brackets(s):
            if s.startswith('['):
                s = s[1:]
            if s.endswith(']'):
                s = s[:-1]
            return s

        def on_array_change(change):
            text = remove_square_brackets(array_box.value)
            try:
                self.data = [int(n) for n in text.split(',')]
            except:
                return
            if ( self.algo == 'CompleteEnumeration' ):
                self.frames = self._subarray_sum_impl_display(self.data)
            else:
                self.frames = self._kadane_impl_display(self.data)
            self.n_frames = len(self.frames)-1
            play.max = self.n_frames
            slider.max = self.n_frames
            play.value = 0
            on_value_change({'new':0})

        def on_algo_change(change):
            self.algo = change['new']
            on_array_change({'new':array_box.value})

        def incr(name):
            counter.value += 1 if counter.value < len(self.frames)-1 else 0

        def decr(name):
            counter.value -= 1 if counter.value > 0 else 0

        class Counter(widgets.DOMWidget):
            value = CInt(0, sync=True)  

        counter = Counter()

        play = widgets.Play(
            value=0,
            min=0,
            max=len(self.frames)-1,
            step=1,
            interval=self.delay,
            description="Press play",
            disabled=False
        )
        slider = widgets.IntSlider(
            min=0,
            max=len(self.frames)-1,
            readout=False
        )

        array_box_layout = widgets.Layout(width='500px')
        array_box = widgets.Text(description='List: ', 
                                 style={'description_width':'100px'}, 
                                 layout=array_box_layout, 
                                 value=('[ '+', '.join(str(x) for x in self.data))+']'
                                )
        
        delay_box_layout = widgets.Layout(width='200px')
        delay_box = widgets.IntText(value=200,
                                    default=200,
                                    description='Delay (in ms): ',
                                    style={'description_width':'100px'},
                                    disabled=False,
                                    layout=delay_box_layout
                                   )

        algo_select_layout = widgets.Layout(width='300px')
        algo_select = widgets.Dropdown(
            value='CompleteEnumeration',
            placeholder='Choose Algo: ',
            options=['CompleteEnumeration', 'Kadane'],
            description='Algorithm: ',
            style={'description_width':'100px'}, 
            ensure_option=True,
            disabled=False,
            layout=algo_select_layout
        )

        layout = widgets.Layout(width='40px')
        button_incr = widgets.Button(description='>',layout=layout)
        button_decr = widgets.Button(description='<',layout=layout)
        button_incr.on_click(incr)
        button_decr.on_click(decr)

        widgets.Dropdown.value.tag(sync=True)

        slider.observe(on_value_change, 'value')
        array_box.observe(on_array_change, 'value')
        algo_select.observe(on_algo_change, 'value')
        on_value_change({'new':0})
        widgets.jslink((play, 'value'), (slider, 'value'))
        widgets.jslink((delay_box, 'value'), (play, 'interval'))
        widgets.jslink((slider, 'value'), (counter, 'value'))

        title = widgets.HTML(markdown.markdown("""# Subarray Sum Problem"""))
        subtitle = widgets.HTML(markdown.markdown("""### Input Data"""))
        algo_title = widgets.HTML(markdown.markdown("""# Visualization"""))
        input_box = widgets.VBox([array_box, delay_box ,algo_select])
        vb2 = widgets.HBox([play,button_decr,button_incr])
        box_control = widgets.VBox([vb2,slider])
        fig = widgets.VBox([title,subtitle,input_box,algo_title,box_control,out])

        input_box.add_class("input_box")
        box_control.add_class("box_control")
        algo_title.add_class("algo_title")
        title.add_class("pagetitle")
        #delay_box.add_class("delay_box")
        fig.add_class("fullpage")

        customHTML= widgets.HTML(
            """<style>\
             @import url('https://fonts.googleapis.com/css?family=Raleway:300,400,700&display=swap');\
            .pagetitle { font-family: \"Raleway\", sans-serif !important; font-size: 150%; color: rgb(185,15,84); } \
            .fullpage { min-width: 700px; max-width: 1000px;} \
            .input_box {margin-left: 10px;margin-bottom: 10px;} \
            .box_control { margin-left: 0px; margin-bottom: 20px; margin-top: 0px; } \
            .delay_box { margin-left: 10px; } \
            .widget-label { font-size: 120%; min-width: 12ex;}\
            .widget-input { font-size: 220%; }\
            .title { font-size: 220%; margin-bottom:20px; }\
            .algo_title { font-size: 120%; margin-bottom:20px; margin-top:20px; }\
            </style>"""
        )

        res = widgets.VBox([customHTML,fig])
        return res
    

class SubarraySumRuntime(widgets.DOMWidget):
    def __init__(self):
        self.data = [random.randrange(0, 50, 1) for i in range(100)]
        start = time.time()
        self.subarray_sum_bf(self.data)
        end = time.time()
        self.runtime_ce = end-start
        start = time.time()
        self.subarray_sum_kadane(self.data)
        end = time.time()
        self.runtime_dp = end-start



    def subarray_sum_bf(self,data):
        max_sum = 0
        length = len(data)
        for i in range(length):
            cur_sum = 0
            for j in range(i,length):
                cur_sum += data[j]
                if ( cur_sum > max_sum ) :
                    max_sum = cur_sum
        return max_sum

    def subarray_sum_kadane(self,data):
        max_sum = 0
        cur_sum = 0
        length = len(data)
        for i in range(length):
            if ( cur_sum+data[i] < 0 ):
                cur_sum = 0
            else:
                cur_sum = cur_sum+data[i]
            if ( cur_sum > max_sum ):
                max_sum = cur_sum
        return max_sum
        
    def display_res(self):
        colors = ['#ddd','#f99','#cde','#cc0']
        content = ""
        content += "<style>"
        content += "table.arr { float: left; }"
        content += "td.large { font-size:200%; }"
        content += "</style>"
        content += '<div><table width=100% class="arr"><tr>'
        content += '<td class="large">Complete Enumeration: </td>'
        start = time.time()
        self.subarray_sum_bf(self.data)
        end = time.time()
        diff = end-start
        content += '<td class="large">'+"{0:0.5f}".format(diff)+'</td>'
        content += '</tr>'
        content += '<td class="large">Kadane\'s Algorithm: </td>'
        start = time.time()
        self.subarray_sum_kadane(self.data)
        end = time.time()
        diff = end-start
        content += '<td class="large">'+"{0:0.5f}".format(diff)+'</td>'
        content += '</table></div>'

        return content
    
    def display_algo(self):

        out = widgets.Output()

        def new_random_array(n):
            self.data = [random.randrange(0, 50, 1) for i in range(n)]
            array_box.value=('[ '+', '.join(str(x) for x in self.data))+']'

        def on_array_change(change):
            text = remove_square_brackets(array_box.value)
            try:
                self.data = [int(n) for n in text.split(',')]
            except:
                return

            elements.value = len(self.data)
            with out: 
                clear_output(wait=True)
                res = self.display_res()
                display(HTML(res))

        def remove_square_brackets(s):
            if s.startswith('['):
                s = s[1:]
            if s.endswith(']'):
                s = s[:-1]
            return s

        button = widgets.Button(
            description='Recompute', 
            button_style='success', 
            layout = widgets.Layout(width='150px')
        )
        elements = widgets.BoundedIntText(
            style={'description_width':'100px'}, 
            value=100,
            min=0,
            max=10000,
            description='# Elements:',
            disabled=False,
            layout = widgets.Layout(width='200px')
        )

        array_box_layout = widgets.Layout(width='700px')
        array_box = widgets.Text(description='List: ', 
                                 style={'description_width':'100px'}, 
                                 layout=array_box_layout, 
                                 value=('[ '+', '.join(str(x) for x in self.data))+']'
                                )
        
        array_box.observe(on_array_change, 'value')
        on_array_change({'new':array_box.value})

        button.on_click(lambda btn: new_random_array(elements.value))

        title1 = widgets.HTML(markdown.markdown("""# Subarray Sum Problem"""))
        title2 = widgets.HTML(markdown.markdown("""# Runtime"""))
        subtitle = widgets.HTML(markdown.markdown("""### Input Data"""))

        algo_title = widgets.HTML(markdown.markdown("""# Runtime"""))
        fig = widgets.VBox([title1,title2,subtitle,widgets.HBox([elements,button]),algo_title,array_box,out])

        algo_title.add_class("algo_title")
        title1.add_class("pagetitle_t")
        title2.add_class("pagetitle_b")
        fig.add_class("fullpage")

        customHTML= widgets.HTML(
            """<style>\
             @import url('https://fonts.googleapis.com/css?family=Raleway:300,400,700&display=swap');\
            .pagetitle_t { font-family: \"Raleway\", sans-serif !important; font-size: 150%; color: rgb(185,15,84); margin-bottom: -30px;} \
            .pagetitle_b { font-family: \"Raleway\", sans-serif !important; font-size: 150%; color: rgb(185,15,84); margin-left: 30px;} \
            .fullpage { min-width: 700px; max-width: 1000px;} \
            .input_box {margin-left: 10px;margin-bottom: 10px;} \
            .box_control { margin-left: 0px; margin-bottom: 20px; margin-top: 0px; } \
            .delay_box { margin-left: 10px; } \
            .widget-label { font-size: 120%; min-width: 12ex;}\
            .widget-input { font-size: 220%; }\
            .title { font-size: 220%; margin-bottom:20px; }\
            .algo_title { font-size: 120%; margin-bottom:20px; margin-top:20px; }\
            </style>"""
        )

        res = widgets.VBox([customHTML,fig])
        return res