
import ipywidgets as widgets
from IPython.display import HTML, display, clear_output
import markdown 
from traitlets import CInt, CUnicode


def wrap_table_ms(x):
    content = ""
    content += '<style>td.largelist { font-size:150%; }'
    content += 'td div{width:50px;height:27px;overflow:hidden;word-wrap:break-word;}'
    content += '</style><table>'
    content += x
    content +='</table>'
    return content


def MergeSort_impl(data,l,u,init_rows):
    frames = []
    frames.append(wrap_table_ms(init_rows+mergesort_display(data,[l,u],[],[],[],[])))
    if ( u-l <= 0 ):
        return frames,init_rows
    m = l+int((u-l)/2)
    frames.append(wrap_table_ms(init_rows+mergesort_display(data,[l,m],[m+1,u],[],[],[])))
    f,i = MergeSort_impl(data,l,m,init_rows)
    frames += f
    init_rows = i
    f,i = MergeSort_impl(data,m+1,u,init_rows)
    frames += f
    init_rows = i
    i = l
    j = m+1
    temp = []
    data_temp = data.copy()
    for k in range(l,u+1):
        temp.append(data[k])
    k = l
    frames.append(wrap_table_ms(init_rows
                             +mergesort_display(data_temp,[l,m],[m+1,u],[],[],[])
                             +mergesort_display(data,[l,u],[],[],[],[k,u])))
    while ( i<=m and j<=u ):
        frames.append(wrap_table_ms(init_rows
                                 +mergesort_display(data_temp,[l,m],[m+1,u],[i,j],[3,3],[])
                                 +mergesort_display(data,[l,u],[],[],[],[k,u])))
        if ( temp[i-l] <= temp[j-l] ):
            data[k] = temp[i-l]
            frames.append(wrap_table_ms(init_rows
                                     +mergesort_display(data_temp,[l,m],[m+1,u],[i],[2],[])
                                     +mergesort_display(data,[l,u],[],[],[],[k,u])))
            i += 1
        else:
            data[k] = temp[j-l]
            frames.append(wrap_table_ms(init_rows
                                     +mergesort_display(data_temp,[l,m],[m+1,u],[j],[2],[])
                                     +mergesort_display(data,[l,u],[],[],[],[k,u])))
            j += 1
        k += 1
        frames.append(wrap_table_ms(init_rows
                                 +mergesort_display(data_temp,[l,m],[m+1,u],[],[],[])
                                 +mergesort_display(data,[l,u],[],[],[],[k,u])))        
    while ( i <= m ):
        data[k] = temp[i-l]
        frames.append(wrap_table_ms(init_rows
                                 +mergesort_display(data_temp,[l,m],[m+1,u],[i],[2],[])
                                 +mergesort_display(data,[l,u],[],[],[],[k,u])))
        i += 1
        k += 1
        frames.append(wrap_table_ms(init_rows
                                 +mergesort_display(data_temp,[l,m],[m+1,u],[],[],[])
                                 +mergesort_display(data,[l,u],[],[],[],[k,u])))
    while ( j <= u ):
        data[k] = temp[j-l]
        frames.append(wrap_table_ms(init_rows
                                 +mergesort_display(data_temp,[l,m],[m+1,u],[j],[2],[])
                                 +mergesort_display(data,[l,u],[],[],[],[k,u])))
        j += 1
        k += 1
        frames.append(wrap_table_ms(init_rows
                                 +mergesort_display(data_temp,[l,m],[m+1,u],[],[],[])
                                 +mergesort_display(data,[l,u],[],[],[],[k,u])))
    init_rows += mergesort_display(data_temp,[l,m],[m+1,u],[],[],[])+mergesort_display(data,[l,u],[],[],[],[])
    #frames.append(wrap_table_ms(init_rows))
    return frames,init_rows

def mergesort_display(data,active,active_r,hi,hc,suppress):
    colors = ['#f99','#50B695','#E9503E','#C9308E','#DCDCDC','#FFE05C']
    clear_output(wait=True)
    content = ""
    content += '<tr>'
    for i in range(len(data)):
        content += '<td  class="largelist" '
        content += ' style="background-color:'
        if ( i>= active[0] and i <=active[1] or (len(active_r)>1 and (i>= active_r[0] and i <=active_r[1])) ):
            if ( hi.count(i) ):
                content += colors[hc[hi.index(i)]]
            else:
                if ( len(active_r)>1 and (i>= active_r[0] and i <=active_r[1]) ):
                    content += colors[5]
                else:
                    content += colors[1]                
        else:
            content += colors[4]
        content += '"'
        content += '><div>'
        if ( len(suppress) <= 0 or i< suppress[0] or i > suppress[1] ):
            content += str(data[i])
        content += '</div></td>'
    content += '</tr>'
    return content

def MergeSort(A):
    x = [ n for n in A]
    frames,i=MergeSort_impl(x,0,len(x)-1,"")
    return frames


def DisplayMergeSort():
    data = [ 5, 4, 3, 7, 12, 5, 6, 0]

    class FrameData(widgets.DOMWidget):
        frames = MergeSort(data)
        n_frames = CInt(len(frames), sync=True)
        current_frame = CInt(0, sync=True)
        data_string = CUnicode(('[ '+', '.join(str(x) for x in data))+']', sync=True)

    class Counter(widgets.DOMWidget):
        value = CInt(0, sync=True)

    def print_content():
        text = remove_square_brackets(frameData.data_string)
        try:
            data = [int(n) for n in text.split(',')]
        except:
            return
        frameData.frames,i = MergeSort_impl(data,0,len(data)-1,"")
        on_value_change({'new':0})
        frameData.n_frames = len(frameData.frames)-1
        frameData.current_frame = 0

    def remove_square_brackets(s):
        if s.startswith('['):
            s = s[1:]
        if s.endswith(']'):
            s = s[:-1]
        return s

    def incr(name):
        counter.value += 1 if counter.value < frameData.n_frames-1 else 0

    def decr(name):
        counter.value -= 1 if counter.value > 0 else 0

    def on_value_change(change):
        with out: 
            clear_output(wait=True)
            display(HTML(frameData.frames[change['new']]))

    def on_array_change(change):
        print_content()
    
    counter = Counter()
    frameData = FrameData()
    
    widgets.Dropdown.value.tag(sync=True)

    delay_box_layout = widgets.Layout(width='200px')
    delay_box = widgets.IntText(value=500,
                                default=500,
                                description='Delay (in ms): ',
                                style={'description_width':'initial'},
                                disabled=False,
                                layout=delay_box_layout
                               )

    play = widgets.Play(
            value=0,
            min=0,
            max=frameData.n_frames-1,
            step=1,
            interval=delay_box.value,
            description="Press play",
            disabled=False
        )
    slider = widgets.IntSlider(
            min=0,
            max=frameData.n_frames-1,
            readout=False
        )

    array_box_layout = widgets.Layout(width='500px')
    array_box = widgets.Text(description='List: ', 
                             style={'description_width':'initial'}, 
                             layout=array_box_layout, value=('[ '+', '.join(str(x) for x in data))+']')

    title_w = widgets.Label("MergeSort")

    out = widgets.Output()

    on_value_change({'new':0})
    print_content()

    layout = widgets.Layout(width='40px')
    button_incr = widgets.Button(description='>',layout=layout)
    button_decr = widgets.Button(description='<',layout=layout)
    button_incr.on_click(incr)
    button_decr.on_click(decr)

    array_box.observe(on_array_change,'value')
    slider.observe(on_value_change, 'value')

    widgets.jslink((delay_box, 'value'), (play, 'interval'))
    widgets.jslink((play, 'value'), (slider, 'value'))
    widgets.jslink((slider, 'value'), (counter, 'value'))
    widgets.jslink((play, 'max'), (frameData, 'n_frames'))
    widgets.jslink((slider, 'max'), (frameData, 'n_frames'))
    widgets.jslink((slider, 'value'), (frameData, 'current_frame'))
    widgets.jslink((array_box, 'value'), (frameData, 'data_string'))

    title = widgets.HTML(markdown.markdown("""# MergeSort"""))
    subtitle = widgets.HTML(markdown.markdown("""### Input Data"""))
    
    box_play   = widgets.HBox([play, button_decr,button_incr])
    box_slider = widgets.HBox([slider])
    box_control = widgets.VBox([box_play,box_slider])
    fig = widgets.VBox([out])
    input_box = widgets.VBox([title,subtitle,array_box,delay_box,box_control],layout=widgets.Layout(align_items='flex-start'))

    input_box.add_class("input_box")
    box_control.add_class("box_control")
    display(HTML(
         "<style>\
        .input_box {margin-left: 10px;margin-bottom: 100px; margin-right:20px;} \
        .box_control { margin-right: 50px; margin-top: 50px; } \
        .widget-label { font-size: 120%; min-width: 12ex;}\
        .widget-input { font-size: 220%; }\
        </style>"
    ))

    display(widgets.HBox([input_box,fig]))